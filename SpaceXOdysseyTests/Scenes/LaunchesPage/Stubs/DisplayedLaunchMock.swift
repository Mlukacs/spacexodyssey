//
//  DisplayedLaunchMock.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 28/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

@testable import SpaceXOdyssey

extension LaunchesPage.DisplayedLaunch {
    static func makeStub(_ flightNumber: Int = 1, _ name: String = "Test name", _ date: String = "Date string", _ imgUrl: String? = nil) -> LaunchesPage.DisplayedLaunch {
        return LaunchesPage.DisplayedLaunch(
            flightNumber: flightNumber,
            name: name,
            date: date,
            imgUrl: imgUrl
        )
    }
}
