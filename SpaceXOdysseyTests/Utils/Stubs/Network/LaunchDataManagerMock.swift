//
//  LaunchDataManagerMock.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 28/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import PromiseKit
@testable import SpaceXOdyssey

final class LaunchesNetworkManagerMock: LaunchesDataManager {
    var getLaunchesCalled = false
    var getLaunchCalled = false
    
    func getLaunches(limit: Int?, offset: Int?, _ debugMode: Bool) -> Promise<Launches> {
        getLaunchesCalled = true

        return APIManager.callApi(LaunchAPI.getAllLaunches(limit: nil, offset: nil), dataReturnType: Launches.self, test: true, debugMode: debugMode)
    }
    
    func getLaunch(flightNumber: Int, _ debugMode: Bool) -> Promise<Launch> {
        getLaunchCalled = true
        return APIManager.callApi(LaunchAPI.getLaunch(flightNumber: flightNumber), dataReturnType: Launch.self, test: true, debugMode: debugMode)
    }
}

enum testError: Error {
    case fakeError
}

final class LaunchesNetworkManagerMockError: LaunchesDataManager {

    func getLaunches(limit: Int?, offset: Int?, _ debugMode: Bool) -> Promise<Launches> {
        return Promise(error: testError.fakeError)
    }
    
    func getLaunch(flightNumber: Int, _ debugMode: Bool) -> Promise<Launch> {
        return Promise(error: LaunchErrors.couldNotLoadLaunches(error: "test cannot get data for launch"))
    }
}

