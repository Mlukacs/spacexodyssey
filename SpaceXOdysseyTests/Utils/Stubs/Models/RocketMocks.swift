//
//  RocketMocks.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 19/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

@testable import SpaceXOdyssey

extension LaunchRocket {
    static let validLaunchRocket: JSONValue = [
        "rocket_id":"falcon1",
        "rocket_name":"Falcon 1",
        "rocket_type":"Merlin A",
        "first_stage": FirstStage.validFirstStage,
        "second_stage": SecondStage.validSecondStage,
        "fairings": Fairings.validFairings
    ]
}

extension FirstStage {
    static let validFirstStage: JSONValue = [
        "cores":[
            Core.validCore
        ]
    ]
}

extension SecondStage {
    static let validSecondStage: JSONValue = [
        "block":5,
        "payloads":[
            Payload.validPayload
        ]
    ]
}

extension Fairings {
    static let validFairings: JSONValue = [
        "reused":false,
        "recovery_attempt":false,
        "recovered":false,
        "ship":"ship test"
    ]
}
