//
//  LaunchSite.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 19/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

@testable import SpaceXOdyssey

extension LaunchSite {
    static let validLaunchSite : JSONValue = [
        "site_id":"ccafs_slc_40",
        "site_name":"CCAFS SLC 40",
        "site_name_long":"Cape Canaveral Air Force Station Space Launch Complex 40"
    ]
}

