//
//  LaunchMocks.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 19/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

@testable import SpaceXOdyssey

extension Launch {
    static let validLaunch : JSONValue = [
        "flight_number": 60,
        "mission_name": "TESS",
        "mission_id": [],
        "launch_year": "2018",
        "launch_date_unix": 1524091860,
        "launch_date_utc": "2018-04-18T22:51:00.000Z",
        "launch_date_local": "2018-04-18T18:51:00-04:00",
        "is_tentative": false,
        "tentative_max_precision": "hour",
        "tbd": false,
        "rocket": LaunchRocket.validLaunchRocket,
        "ships": [
            "GOPURSUIT",
            "GOQUEST",
            "HAWK",
            "OCISLY"
        ],
        "telemetry": Telemetry.validTelemetry,
        "launch_site": LaunchSite.validLaunchSite,
        "launch_success": true,
        "links": Links.validLinks,
        "details": "Part of the Explorers program, this space telescope is intended for wide-field search of exoplanets transiting nearby stars. It is the first NASA high priority science mission launched by SpaceX. It was the first time SpaceX launched a scientific satellite not primarily intended for Earth observations. The second stage placed it into a high-Earth elliptical orbit, after which the satellite's own booster will perform complex maneuvers including a lunar flyby, and over the course of two months, reach a stable, 2:1 resonant orbit with the Moon. In January 2018, SpaceX received NASA's Launch Services Program Category 2 certification of its Falcon 9 'Full Thrust', certification which is required for launching medium risk missions like TESS. It was the last launch of a new Block 4 booster, and marked the 24th successful recovery of the booster. An experimental water landing was performed in order to attempt fairing recovery.",
        "upcoming": false,
        "static_fire_date_utc": "2018-04-11T18:30:00.000Z",
        "static_fire_date_unix": 1523471400
    ]
}

extension Telemetry {
    static let validTelemetry: JSONValue = [
        "flight_club":"https://www.flightclub.io/result/2d?code=T18V"
    ]
}

extension Links {
    static let validLinks: JSONValue = [
        "mission_patch":"https://images2.imgbox.com/ba/db/3plcm5IB_o.png",
        "mission_patch_small":"https://images2.imgbox.com/2d/d2/jStsqeLC_o.png",
        "reddit_campaign":"https://www.reddit.com/r/spacex/comments/95cte4/telstar_18v_apstar_5c_launch_campaign_thread/",
        "reddit_launch":"https://www.reddit.com/r/spacex/comments/9e7bmq/rspacex_telstar_18v_official_launch_discussion/",
        "reddit_recovery":"https://www.reddit.com/r/spacex/comments/9erxlh/telstar_18_vantage_recovery_thread/",
        "reddit_media":"https://www.reddit.com/r/spacex/comments/9ebkqw/rspacex_telstar_18v_media_thread_videos_images/",
        "presskit":"https://www.spacex.com/sites/spacex/files/telstar18vantagepresskit.pdf",
        "article_link":"https://spaceflightnow.com/2018/09/10/spacex-telesat-achieve-repeat-success-with-midnight-hour-launch/",
        "wikipedia":"https://en.wikipedia.org/wiki/Telstar_18V",
        "video_link":"https://www.youtube.com/watch?v=Apw3xqwsG1U",
        "flickr_images":[
            "https://farm2.staticflickr.com/1878/43690848045_492ef182dd_o.jpg",
            "https://farm2.staticflickr.com/1856/43881229604_6d42e838b6_o.jpg",
            "https://farm2.staticflickr.com/1852/43881223704_93777e34af_o.jpg",
            "https://farm2.staticflickr.com/1841/43881217094_558b7b214e_o.jpg",
            "https://farm2.staticflickr.com/1869/43881193934_423eff8c86_o.jpg"
        ]
    ]
}

