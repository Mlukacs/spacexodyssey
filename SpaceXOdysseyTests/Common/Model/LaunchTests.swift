//
//  LaunchTests.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 20/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

@testable import SpaceXOdyssey
import Quick
import Nimble

class LaunchTests: QuickSpec {
    
    override func spec() {
        describe("Launch Test") {
            var launch: Launch!
            var validLaunch: JSONValue!
            
            beforeEach {
                validLaunch = Launch.validLaunch
            }
            
            it("should be a valide launch") {
                launch = try! validLaunch.decode()
                
                expect(launch).to(beAKindOf(Launch.self))
                expect(launch.missionName) == "TESS"
                expect(launch.flightNumber) == 60
                expect(launch.upcoming) == false
            }
        }
    }
}

