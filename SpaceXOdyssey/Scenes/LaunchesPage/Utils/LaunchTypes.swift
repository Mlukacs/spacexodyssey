//
//  LaunchTypes.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 25/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

enum LaunchType: Int {
    case passed
    case upcoming
    
    static func getType(_ type: Int) -> LaunchType {
        switch type {
        case 0:
            return .passed
        default:
            return .upcoming
        }
    }
}
