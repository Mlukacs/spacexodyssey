//
//  ElementSetup.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 13/01/2019.
//  Copyright © 2019 mdev. All rights reserved.
//

import UIKit

public protocol Setup: NSObjectProtocol {}

extension Setup {
    @discardableResult public func setUp(_ block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
}

extension UIView: Setup {}
