//
//  OrbitParams.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 18/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

struct OrbitParams: Codable {
    let referenceSystem: ReferenceSystem?
    let regime: Regime?
    let longitude, semiMajorAxisKM, eccentricity, periapsisKM: Double?
    let apoapsisKM, inclinationDeg, periodMin, lifespanYears: Double?
    let epoch: String?
    let meanMotion, raan: Double?
    let argOfPericenter, meanAnomaly: Double?
    
    enum CodingKeys: String, CodingKey {
        case referenceSystem = "reference_system"
        case regime, longitude
        case semiMajorAxisKM = "semi_major_axis_km"
        case eccentricity
        case periapsisKM = "periapsis_km"
        case apoapsisKM = "apoapsis_km"
        case inclinationDeg = "inclination_deg"
        case periodMin = "period_min"
        case lifespanYears = "lifespan_years"
        case epoch
        case meanMotion = "mean_motion"
        case raan
        case argOfPericenter = "arg_of_pericenter"
        case meanAnomaly = "mean_anomaly"
    }
}

enum ReferenceSystem: String, Codable {
    case geocentric = "geocentric"
    case heliocentric = "heliocentric"
    case highlyElliptical = "highly-elliptical"
    case selenocentric = "selenocentric"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try ReferenceSystem(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}

enum Regime: String, Codable {
    case geostationary = "geostationary"
    case geosynchronous = "geosynchronous"
    case highEarth = "high-earth"
    case l1Point = "L1-point"
    case lowEarth = "low-earth"
    case subOrbital = "sub-orbital"
    case sunSynchronous = "sun-synchronous"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try Regime(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}
