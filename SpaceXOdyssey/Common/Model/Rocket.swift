//
//  Rocket.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 18/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

struct LaunchRocket: Codable {
    let rocketID: RocketID
    let rocketName: RocketName
    let rocketType: RocketType
    let firstStage: FirstStage
    let secondStage: SecondStage
    let fairings: Fairings?
    
    enum CodingKeys: String, CodingKey {
        case rocketID = "rocket_id"
        case rocketName = "rocket_name"
        case rocketType = "rocket_type"
        case firstStage = "first_stage"
        case secondStage = "second_stage"
        case fairings
    }
}

enum RocketID: String, Codable {
    case falcon1
    case falcon9
    case falconheavy
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try RocketID(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}

enum RocketName: String, Codable {
    case falcon1 = "Falcon 1"
    case falcon9 = "Falcon 9"
    case falconHeavy = "Falcon Heavy"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try RocketName(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}

enum RocketType: String, Codable {
    case ft = "FT"
    case merlinA = "Merlin A"
    case merlinC = "Merlin C"
    case v10 = "v1.0"
    case v11 = "v1.1"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try RocketType(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}

struct FirstStage: Codable {
    let cores: [Core]
}

struct SecondStage: Codable {
    let block: Int?
    let payloads: [Payload]
}

struct Fairings: Codable {
    let reused: Bool
    let recoveryAttempt, recovered: Bool?
    let ship: String?
    
    enum CodingKeys: String, CodingKey {
        case reused
        case recoveryAttempt = "recovery_attempt"
        case recovered, ship
    }
}
