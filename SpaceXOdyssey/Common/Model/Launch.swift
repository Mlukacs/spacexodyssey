//
//  Launch.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 18/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Foundation

typealias Launches = [Launch]

struct Launch: Codable {
    let flightNumber: Int
    let missionName: String
    let missionID: [String]
    let upcoming: Bool
    let launchYear: String
    let launchDateUnix: Int
    let launchDateUTC: String
    let launchDateLocal: Date
    let isTentative: Bool
    let tentativeMaxPrecision: TentativeMaxPrecision
    let tbd: Bool
    let rocket: LaunchRocket
    let ships: [String]
    let telemetry: Telemetry
    let launchSite: LaunchSite
    let launchSuccess: Bool?
    let launchFailureDetails: LaunchFailureDetails?
    let links: Links
    let details, staticFireDateUTC: String?
    let staticFireDateUnix: Int?
    
    enum CodingKeys: String, CodingKey {
        case flightNumber = "flight_number"
        case missionName = "mission_name"
        case missionID = "mission_id"
        case upcoming
        case launchYear = "launch_year"
        case launchDateUnix = "launch_date_unix"
        case launchDateUTC = "launch_date_utc"
        case launchDateLocal = "launch_date_local"
        case isTentative = "is_tentative"
        case tentativeMaxPrecision = "tentative_max_precision"
        case tbd, rocket, ships, telemetry
        case launchSite = "launch_site"
        case launchSuccess = "launch_success"
        case launchFailureDetails = "launch_failure_details"
        case links, details
        case staticFireDateUTC = "static_fire_date_utc"
        case staticFireDateUnix = "static_fire_date_unix"
    }
}

struct LaunchFailureDetails: Codable {
    let time: Int
    let altitude: Int?
    let reason: String
}

struct Links: Codable {
    let missionPatch, missionPatchSmall: String?
    let articleLink: String?
    let wikipedia, videoLink: String?
    let flickrImages: [String]
    let presskit: String?
    let redditCampaign: String?
    let redditLaunch: String?
    let redditRecovery, redditMedia: String?
    
    enum CodingKeys: String, CodingKey {
        case missionPatch = "mission_patch"
        case missionPatchSmall = "mission_patch_small"
        case articleLink = "article_link"
        case wikipedia
        case videoLink = "video_link"
        case flickrImages = "flickr_images"
        case presskit
        case redditCampaign = "reddit_campaign"
        case redditLaunch = "reddit_launch"
        case redditRecovery = "reddit_recovery"
        case redditMedia = "reddit_media"
    }
    
    var youTubeVideoId: String? {
        guard let link = videoLink else { return nil }
        guard let url = URL(string: link) else { return nil }
        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else { return nil }
        guard let host = components.host else { return nil }
        guard host.contains("youtube.com") else { return nil }
        return components.queryItems?.first(where: { $0.name == "v" })?.value
    }
}

struct Telemetry: Codable {
    let flightClub: String?
    
    enum CodingKeys: String, CodingKey {
        case flightClub = "flight_club"
    }
}

enum TentativeMaxPrecision: String, Codable {
    case day
    case hour
    case month
    case quarter
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try TentativeMaxPrecision(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}
