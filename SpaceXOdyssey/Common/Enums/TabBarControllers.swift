//
//  TabBarControllers.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 14/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import SwiftIconFont

/// Enum containing the tabbar controllers
///
/// - home: Main TabBar page
/// - chart: User charts
/// - account: User account controller
enum TabBarControllers: String, CaseIterable {
    case launches
    case rockets
    case roadster
    case about
    
    /// Return the image associated with the current value of TabBarControllers
    ///
    /// - Parameter selected: Boolean taht determinates the state of image
    /// - Returns: The  image to display in the tabbar
    func getImage(selected: Bool = false) -> UIImage {
        switch self {
        case .launches:
            return UIImage(from: .fontAwesome, code: "rocket", textColor: selected ? Style.Color.MainGray : .lightGray, backgroundColor: .clear, size: Style.Size.tabBarIcon)
        case .rockets:
            return UIImage(from: .fontAwesome, code: "spaceshuttle", textColor: selected ? Style.Color.MainGray : .lightGray, backgroundColor: .clear, size: Style.Size.tabBarIcon)
        case .roadster:
            return UIImage(from: .fontAwesome, code: "car", textColor: selected ? Style.Color.MainGray : .lightGray, backgroundColor: .clear, size: Style.Size.tabBarIcon)
        case .about:
            return UIImage(from: .fontAwesome, code: "user", textColor: selected ? Style.Color.MainGray : .lightGray, backgroundColor: .clear, size: Style.Size.tabBarIcon)
            
        }
    }
    
    /// Return a viewController associated with the current value of TabBarControllers
    ///
    /// - Returns: ViewController to display in tabbar
    func getViewController() -> UIViewController {
        switch self {
        case .launches:
            return LaunchesViewController()
        case .rockets:
            return UIViewController()
        case .roadster:
            return  UIViewController()
        case .about:
            return  UIViewController()
            
        }
    }
    
    /// Help for formatted names of controllers in tabbar
    ///
    /// - Returns: Uppercase controllers description names
    func getName() -> String {
        switch self {
        case .launches:
            return "Launches".uppercased()
        case .rockets:
            return "Rockets".uppercased()
        case .roadster:
            return "Rodster".uppercased()
        case .about:
            return "About".uppercased()
        }
    }
}
