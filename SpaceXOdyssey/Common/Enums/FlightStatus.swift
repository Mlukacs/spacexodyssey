//
//  FlightStatus.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 13/01/2019.
//  Copyright © 2019 mdev. All rights reserved.
//

enum FlightStatus: String {
    case success
    case failure
    case upcoming
}
