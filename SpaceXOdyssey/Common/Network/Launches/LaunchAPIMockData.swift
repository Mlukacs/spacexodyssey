//
//  LauncheAPIMockData.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 18/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Foundation

extension LaunchAPI {
    var sampleData: Data {
        switch self {
        case .getAllLaunches:
            return stubbedResponse("Launches")
        case .getLaunch:
            return stubbedResponse("Launch")
        }
    }
}
