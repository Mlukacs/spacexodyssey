//
//  RocketAPI.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 20/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Moya

enum RocketAPI {
    case getAllRockets(limit: Int?, offset: Int?)
}

extension RocketAPI: TargetType {
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    var path: String {
        switch self {
        case .getAllRockets:
            return "/rockets/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getAllRockets:
            return .get
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getAllRockets:
            return URLEncoding.methodDependent
        }
    }
    
    var task: Task {
        switch self {
        case .getAllRockets(let limit, let offset):
            var params: [String: Any] = [:]
            params["limit"] = limit
            params["offset"] = offset
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }
}
