//
//  Style.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 14/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Colorify

enum Style {
    
    enum Color {
        static let MainGray = Colorify.WetAsphalt
    }
    
    enum Size {
        static let margin: CGFloat = 20.0
        static let tabBarIcon = CGSize(width: 22, height: 22)
        static let topMargin: CGFloat = 64
    }
}
