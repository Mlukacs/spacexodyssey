//
//  UIImageView+Extensions.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 24/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Kingfisher

extension UIImageView {
    func download(image url: String) {
        
        guard let imageURL = URL(string: url) else {
            self.image = UIImage(named: "square-logo")
            return
        }
        let image = UIImage(named: "spaceX")
        self.kf.setImage(with: ImageResource(downloadURL: imageURL), placeholder: image)
    }
}
