//
//  UIView+Utils.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 13/01/2019.
//  Copyright © 2019 mdev. All rights reserved.
//

import UIKit

extension UIView {
    internal func addSubviews(_ subviews: [UIView] ) {
        for view in subviews {
            self.addSubview(view)
        }
    }
    
    class func fromNib<T: UIView>() -> T? {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)?[0] as? T
    }
}
