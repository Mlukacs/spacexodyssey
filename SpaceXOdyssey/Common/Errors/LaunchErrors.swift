//
//  LaunchErrors.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 23/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

enum LaunchErrors: Error {
    case couldNotLoadLaunches(error: String)
}
